import os

from django.shortcuts import render, HttpResponseRedirect, HttpResponse
from django.views.generic import ListView
from django.views import View

from rmpy import rmpy

from .models import *
from .forms import *


def make_name(bin_path):
    trash_path = os.path.join(bin_path, "files")
    info_path = os.path.join(bin_path, "info")
    return trash_path, info_path


class MainView(ListView):
    model = Task
    context_object_name = "tasks"
    template_name = "web_rmpy/index.html"
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        context["bins"] = Bin.objects.all()
        return context


def trash(request, bin_id):
    bin_path = Bin.objects.get(id=bin_id).path
    trash_path, _ = make_name(bin_path)
    trash_list = os.listdir(trash_path)
    context = {"dir": trash_list}
    return render(request, "web_rmpy/trash.html", context)


def recover(request, bin_id):
    bin_path = Bin.objects.get(id=bin_id).path
    trash_path, info_path = make_name(bin_path)
    elements = request.POST.getlist("elems")
    for element in elements:
        task = Task(action="Recover", object=element, status="In progress", bin=bin_path)
        task.save()
        rmpy.recover(element, trash_path, info_path)
        task.status = "OK"
        task.save()
    return HttpResponseRedirect("/" + bin_id)


def delete(request, bin_id):
    if not Task.objects.filter(action="Delete", status="In progress").exists():
        bin_path = Bin.objects.get(id=bin_id).path
        trash_path, info_path = make_name(bin_path)
        target = request.POST.get("target")
        task = Task(action="Delete", object=target, status="In progress", bin=bin_path)
        task.save()
        try:
            rmpy.remove_by_pattern(target, trash_path, info_path)
            task.status = "OK"
        except:
            task.status = "FAILED"
        finally:
            task.save()
        return HttpResponseRedirect("/" + bin_id)
    else:
        return HttpResponse("Error")


def empty(request, bin_id):
    bin_path = Bin.objects.get(id=bin_id).path
    trash_path, info_path = make_name(bin_path)
    task = Task(action="Empty", status="In progress", bin=bin_path)
    task.save()
    rmpy.empty_trash(trash_path, info_path)
    task.status = "OK"
    task.save()
    return HttpResponseRedirect("/" + bin_id)


def regex(request, bin_id):
    if not Task.objects.filter(action="Delete", status="In progress").exists():
        bin_path = Bin.objects.get(id=bin_id).path
        trash_path, info_path = make_name(bin_path)
        tree_path = request.POST.get("path")
        regexp = request.POST.get("regex")
        task = Task(action="Delete", object=regexp, status="In progress", bin=bin_path)
        task.save()
        try:
            rmpy.remove_by_regex(tree_path, regexp, trash_path, info_path)
            task.status = "OK"
        except:
            task.status = "FAILED"
        finally:
            task.save()
        return HttpResponseRedirect("/" + bin_id)
    else:
        return HttpResponse("Error")
