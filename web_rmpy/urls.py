from django.conf.urls import url

from . import views

app_name = "web_rmpy"
urlpatterns = [
    url(r'^$', views.MainView.as_view(), name="bins"),
    url(r'^([0-9]+)/$', views.trash, name="trash"),
    url(r'^([0-9]+)/empty/$', views.empty, name="empty"),
    url(r'^([0-9]+)/regex/$', views.regex, name="regex"),
    url(r'^([0-9]+)/delete/$', views.delete, name="delete"),
    url(r'^([0-9]+)/recover/$', views.recover, name="recover"),
]
