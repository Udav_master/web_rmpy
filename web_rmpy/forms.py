from django import forms

from .validators import *


class DeleteForm(forms.Form):
    path = forms.CharField(max_length=100, validators=[validate_exist])
