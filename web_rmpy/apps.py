from django.apps import AppConfig


class WebRmpyConfig(AppConfig):
    name = 'web_rmpy'
