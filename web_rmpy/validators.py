import os

from django.core.exceptions import ValidationError


def validate_exist(path):
    if not os.path.exists(path):
        raise ValidationError('Path %s not exists' % path)
