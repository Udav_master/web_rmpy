from django.db import models


class Bin(models.Model):
    path = models.CharField(max_length=100, blank=False)

    def __str__(self):
        return self.path


class Task(models.Model):
    action = models.CharField(max_length=40)
    object = models.CharField(max_length=100)
    status = models.CharField(max_length=20)
    bin = models.CharField(max_length=100, blank=False)

    def __str__(self):
        return self.action + " " + "[" + self.status + "]"
